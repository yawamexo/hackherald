package za.co.hackherald.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import za.co.hackherald.config.Utils;
import za.co.hackherald.dtos.HackerPayloadDTO;
import za.co.hackherald.dtos.UpdatesPayloadDTO;
import za.co.hackherald.dtos.UserPayloadDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Service
public class ServiceCalls {
    static Logger logger = LoggerFactory.getLogger(ServiceCalls.class);
    private String serviceName;
    private String suffix = ".json?print=pretty";
    private String emptyObject = "[]";


    @Cacheable("HackerPayloadDTO")
    public HackerPayloadDTO serviceCallItem(Long itemID) {
        Utils utils = new Utils();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Object> request = new HttpEntity<>(emptyObject, headers);
            RestTemplate template = new RestTemplate();
            this.serviceName = utils.getHost() + utils.getVersion() + "item/" + itemID + suffix;
            ResponseEntity<Object> response = template.exchange(this.serviceName, HttpMethod.GET, request, Object.class);
            logger.info("Request : " + this.serviceName);
            logger.info("Response : " + response.toString());
            return utils.mapItemEntityResponse(response);
        } catch (Exception e) {
            //error
        }
        return new HackerPayloadDTO();
    }

    @Cacheable("UserPayloadDTO")
    public UserPayloadDTO serviceCallUser(String id) {
        Utils utils = new Utils();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Object> request = new HttpEntity<>(emptyObject, headers);
            RestTemplate template = new RestTemplate();
            this.serviceName = utils.getHost() + utils.getVersion() + "user/" + id + suffix;
            ResponseEntity<Object> response = template.exchange(this.serviceName, HttpMethod.GET, request, Object.class);
            logger.info("Request : " + this.serviceName);
            logger.info("Response : " + response.toString());
            return utils.mapUserEntityResponse(response);
        } catch (Exception e) {
            //error
        }
        return new UserPayloadDTO();
    }

    public Long serviceCallMaxItem() {
        Utils utils = new Utils();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Object> request = new HttpEntity<>(emptyObject, headers);
            RestTemplate template = new RestTemplate();
            this.serviceName = utils.getHost() + utils.getVersion() + "maxitem" + suffix;
            ResponseEntity<Object> response = template.exchange(this.serviceName, HttpMethod.GET, request, Object.class);
            logger.info("Request : " + this.serviceName);
            logger.info("Response : " + response.toString());
            return Long.parseLong(response.getBody().toString());
        } catch (Exception e) {
            //error
        }
        return 0L;
    }

    public List<Long> serviceCallStories(String storyType) {
//        caters for : 1) topstories   2) newstories  3) beststories  4) askstories  5) showstories  6) jobstories
        Utils utils = new Utils();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Object> request = new HttpEntity<>(emptyObject, headers);
            RestTemplate template = new RestTemplate();
            this.serviceName = utils.getHost() + utils.getVersion() + storyType + suffix;
            ResponseEntity<Object> response = template.exchange(this.serviceName, HttpMethod.GET, request, Object.class);

            ObjectMapper mapper = new ObjectMapper();
            Long[] values = mapper.readValue(response.getBody().toString(), Long[].class);
            logger.info("Request : " + this.serviceName);
            logger.info("Response : " + response.toString());
            return Arrays.asList(values);
        } catch (Exception e) {
            //error
        }
        return new ArrayList<>();
    }

    public UpdatesPayloadDTO serviceCallUpdates() {
        Utils utils = new Utils();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Object> request = new HttpEntity<>(emptyObject, headers);
            RestTemplate template = new RestTemplate();
            this.serviceName = utils.getHost() + utils.getVersion() + "updates" + suffix;
            ResponseEntity<Object> response = template.exchange(this.serviceName, HttpMethod.GET, request, Object.class);
            logger.info("Request : " + this.serviceName);
            logger.info("Response : " + response.toString());
            return utils.mapUpdateEntityResponse(response);
        } catch (Exception e) {
            //error
        }
        return new UpdatesPayloadDTO();
    }
}