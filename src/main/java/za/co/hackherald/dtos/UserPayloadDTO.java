package za.co.hackherald.dtos;

import gherkin.deps.com.google.gson.Gson;

import java.util.List;

public class UserPayloadDTO {
    private String id; //The user's unique username. Case-sensitive. Required.
    private Integer delay; //Delay in minutes between a comment's creation and its visibility to other users.
    private String created; //Creation date of the user, in Unix Time.
    private Long karma; //The user's karma.
    private String about; //The user's optional self-description. HTML.
    private List<Long> submitted; //List of the user's stories, polls and comments.

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getDelay() {
        return delay;
    }

    public void setDelay(Integer delay) {
        this.delay = delay;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Long getKarma() {
        return karma;
    }

    public void setKarma(Long karma) {
        this.karma = karma;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public List<Long> getSubmitted() {
        return submitted;
    }

    public void setSubmitted(List<Long> submitted) {
        this.submitted = submitted;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
