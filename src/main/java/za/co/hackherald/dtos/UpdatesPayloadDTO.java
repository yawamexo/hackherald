package za.co.hackherald.dtos;

import gherkin.deps.com.google.gson.Gson;

import java.util.List;

public class UpdatesPayloadDTO {
    private List<Long> items;
    private List<String> profiles;

    public List<Long> getItems() {
        return items;
    }

    public void setItems(List<Long> items) {
        this.items = items;
    }

    public List<String> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<String> profiles) {
        this.profiles = profiles;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
