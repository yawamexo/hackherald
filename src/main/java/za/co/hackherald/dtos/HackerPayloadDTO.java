package za.co.hackherald.dtos;

import gherkin.deps.com.google.gson.Gson;

import java.util.List;

public class HackerPayloadDTO {

    private Long id; //The item's unique id.
    private boolean deleted; //true if the item is deleted.
    private String type; //The type of item. One of "job", "story", "comment", "poll", or "pollopt".
    private String by; //The username of the item's author.
    private String time; //Creation date of the item, in Unix Time.
    private String text; //The comment, story or poll text. HTML.
    private boolean dead; //true if the item is dead.
    private String parent; //The comment's parent: either another comment or the relevant story.
    private Long poll; //The pollopt's associated poll.
    private List<Long> kids; //The ids of the item's comments, in ranked display order.
    private String url; //The URL of the story.
    private Integer score; //The story's score, or the votes for a pollopt.
    private String title; //The title of the story, poll or job. HTML.
    private List<Long> parts; //A list of related pollopts, in display order.
    private Integer descendants; //In the case of stories or polls, the total comment count.

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public Long getPoll() {
        return poll;
    }

    public void setPoll(Long poll) {
        this.poll = poll;
    }

    public List<Long> getKids() {
        return kids;
    }

    public void setKids(List<Long> kids) {
        this.kids = kids;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Long> getParts() {
        return parts;
    }

    public void setParts(List<Long> parts) {
        this.parts = parts;
    }

    public Integer getDescendants() {
        return descendants;
    }

    public void setDescendants(Integer descendants) {
        this.descendants = descendants;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }


}
