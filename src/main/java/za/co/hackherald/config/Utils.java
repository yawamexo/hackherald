package za.co.hackherald.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import za.co.hackherald.dtos.HackerPayloadDTO;
import za.co.hackherald.dtos.UpdatesPayloadDTO;
import za.co.hackherald.dtos.UserPayloadDTO;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class Utils {

    public String getServiceURL(String serviceName) throws IOException {
        Properties prop = new Properties();
        String propFileName = "application.properties";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        if (Objects.nonNull(inputStream)) {
            prop.load(inputStream);
        }
        return prop.getProperty(serviceName);
    }

    public String getConfigValue(String key) {
        try {
            Properties prop = new Properties();
            String propFileName = "application.properties";
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            if (Objects.nonNull(inputStream)) {
                prop.load(inputStream);
            }
            return prop.getProperty(key);
        } catch (IOException e) {
            //nothing
        }
        return null;
    }

    public int getMax() {
        return Integer.parseInt(getConfigValue("max_story"));
    }

    public String getHost() {
        return getConfigValue("api.server");
    }

    public String getVersion() {
        return getConfigValue("api.version");
    }

    public String getDate(Long value) {

        Date date = new java.util.Date(value * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
        return sdf.format(date);
    }

    public HackerPayloadDTO mapItemEntityResponse(ResponseEntity<Object> response) {

        try {
            HashMap<String, Object> responseBody = (HashMap<String, Object>) response.getBody();
            HackerPayloadDTO hackerPayloadDTO = new HackerPayloadDTO();

            for (String item : responseBody.keySet()) {
                if (item.equalsIgnoreCase("id") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setId(Long.parseLong(value));
                }
                if (item.equalsIgnoreCase("deleted") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setDeleted(Boolean.parseBoolean(value));
                }
                if (item.equalsIgnoreCase("type") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setType(value);
                }
                if (item.equalsIgnoreCase("by") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setBy(value);
                }
                if (item.equalsIgnoreCase("time") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setTime(getDate(Long.parseLong(value)));
                }
                if (item.equalsIgnoreCase("text") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setText(value);
                }
                if (item.equalsIgnoreCase("dead") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setDead(Boolean.parseBoolean(value));
                }
                if (item.equalsIgnoreCase("parent") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setParent(value);
                }
                if (item.equalsIgnoreCase("poll") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setPoll(Long.parseLong(value));
                }

                if (item.equalsIgnoreCase("kids") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    ObjectMapper mapper = new ObjectMapper();
                    Long[] values = mapper.readValue(value, Long[].class);
                    hackerPayloadDTO.setKids(Arrays.asList(values));
                }

                if (item.equalsIgnoreCase("url") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setUrl(value);
                }
                if (item.equalsIgnoreCase("score") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setScore(Integer.parseInt(value));
                }
                if (item.equalsIgnoreCase("title") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setTitle(value);
                }

                if (item.equalsIgnoreCase("parts") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    ObjectMapper mapper = new ObjectMapper();
                    Long[] values = mapper.readValue(value, Long[].class);
                    hackerPayloadDTO.setParts(Arrays.asList(values));
                }

                if (item.equalsIgnoreCase("descendants") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    hackerPayloadDTO.setDescendants(Integer.parseInt(value));
                }
            }
            return hackerPayloadDTO;
        } catch (Exception e) {
            //error
        }
        return null;
    }


    public UserPayloadDTO mapUserEntityResponse(ResponseEntity<Object> response) {
        try {
            HashMap<String, Object> responseBody = (HashMap<String, Object>) response.getBody();
            UserPayloadDTO userPayloadDTO = new UserPayloadDTO();

            for (String item : responseBody.keySet()) {
                if (item.equalsIgnoreCase("id") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    userPayloadDTO.setId(value);
                }
                if (item.equalsIgnoreCase("delay") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    userPayloadDTO.setDelay(Integer.parseInt(value));
                }
                if (item.equalsIgnoreCase("created") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    userPayloadDTO.setCreated(getDate(Long.parseLong(value)));
                }
                if (item.equalsIgnoreCase("karma") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    userPayloadDTO.setKarma(Long.parseLong(value));
                }
                if (item.equalsIgnoreCase("about") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    userPayloadDTO.setAbout(value);
                }
                if (item.equalsIgnoreCase("submitted") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    ObjectMapper mapper = new ObjectMapper();
                    Long[] values = mapper.readValue(value, Long[].class);
                    userPayloadDTO.setSubmitted(Arrays.asList(values));
                }
            }
            return userPayloadDTO;
        } catch (Exception e) {
            //error
        }
        return null;
    }

    public UpdatesPayloadDTO mapUpdateEntityResponse(ResponseEntity<Object> response) {
        try {
            HashMap<String, Object> responseBody = (HashMap<String, Object>) response.getBody();
            UpdatesPayloadDTO updatesPayloadDTO = new UpdatesPayloadDTO();

            for (String item : responseBody.keySet()) {
                if (item.equalsIgnoreCase("items") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    ObjectMapper mapper = new ObjectMapper();
                    Long[] values = mapper.readValue(value, Long[].class);
                    updatesPayloadDTO.setItems(Arrays.asList(values));
                }
                if (item.equalsIgnoreCase("profiles") && Objects.nonNull(responseBody.get(item))) {
                    String value = responseBody.get(item).toString();
                    value = value.replace(",", "\",\"").replace("]", "\"]").replace("[", "[\"");
                    ObjectMapper mapper = new ObjectMapper();
                    String[] values = mapper.readValue(value, String[].class);
                    updatesPayloadDTO.setProfiles(Arrays.asList(values));
                }
            }
            return updatesPayloadDTO;
        } catch (Exception e) {
            //error
        }
        return null;
    }

}
