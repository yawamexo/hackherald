package za.co.hackherald.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.hackherald.services.ServiceCalls;

import java.util.Objects;

@Component
public class ManageItems {

    @Autowired
    private Utils utils;

    @Autowired
    private ServiceCalls serviceCalls;

    public String getItemURL(Long itemID) {
        return utils.getHost() + utils.getVersion() + "item/" + itemID + ".json?print=pretty";
    }

    public String validate(String string) {
        if (Objects.isNull(string) || string.isEmpty()) {
            return "not specified";
        } else {
            return string;
        }
    }

    public String getSourceDetails(Long itemID) {
        return validate(serviceCalls.serviceCallItem(itemID).getTitle());
    }


}
