package za.co.hackherald;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;


/**
 * Created by xigler on 10/14/2017.
 */
@EnableCaching
@SpringBootApplication
public class HackHeraldAPI extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(HackHeraldAPI.class, args);

    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(HackHeraldAPI.class);
    }

}
