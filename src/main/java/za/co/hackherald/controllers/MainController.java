package za.co.hackherald.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import za.co.hackherald.config.ManageItems;
import za.co.hackherald.config.Utils;
import za.co.hackherald.dtos.HackerPayloadDTO;
import za.co.hackherald.dtos.UserPayloadDTO;
import za.co.hackherald.services.ServiceCalls;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@RestController
public class MainController {

    @Autowired
    private ServiceCalls serviceCalls;

    @Autowired
    private ManageItems manageItems;

    @Autowired
    private Utils utils;

    @GetMapping(value = "/")
    public ModelAndView login(Long itemID) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("$time", new Date().toString());
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @GetMapping(value = "/index")
    public ModelAndView index(Long itemID) {
        ModelAndView modelAndView = new ModelAndView();

        HackerPayloadDTO hackerPayloadDTO =
                Objects.isNull(itemID) ? serviceCalls.serviceCallItem(serviceCalls.serviceCallMaxItem())
                        : serviceCalls.serviceCallItem(itemID);

        modelAndView.addObject("$title", hackerPayloadDTO.getTitle());
        modelAndView.addObject("$type", hackerPayloadDTO.getType());
        modelAndView.addObject("$author", hackerPayloadDTO.getBy());
        modelAndView.addObject("$date", hackerPayloadDTO.getTime());
        modelAndView.addObject("$comment", hackerPayloadDTO.getText());
        modelAndView.addObject("$url", hackerPayloadDTO.getUrl());

        if (Objects.nonNull(hackerPayloadDTO.getParent())) {
            modelAndView.addObject("$source", manageItems.getSourceDetails(Long.parseLong(hackerPayloadDTO.getParent())));
            modelAndView.addObject("$sourceid", Long.parseLong(hackerPayloadDTO.getParent()));
        }
        if (Objects.nonNull(hackerPayloadDTO.getKids())) {
            List<HackerPayloadDTO> kids = new ArrayList<>();
            for (Long kid : hackerPayloadDTO.getKids()) {
                if (!serviceCalls.serviceCallItem(kid).isDead() && !serviceCalls.serviceCallItem(kid).isDeleted()) {
                    kids.add(serviceCalls.serviceCallItem(kid));
                }
            }
            modelAndView.addObject("$kids", kids);
        }

        if (hackerPayloadDTO.getType().equalsIgnoreCase("poll")) {
            modelAndView.addObject("$score", hackerPayloadDTO.getScore());
            modelAndView.addObject("$descendants", hackerPayloadDTO.getDescendants());

            if (Objects.nonNull(hackerPayloadDTO.getParts())) {
                List<HackerPayloadDTO> parts = new ArrayList<>();
                for (Long part : hackerPayloadDTO.getParts()) {
                    parts.add(serviceCalls.serviceCallItem(part));
                }
                modelAndView.addObject("$parts", parts);
            }
            modelAndView.setViewName("poll");
            return modelAndView;
        }

        modelAndView.setViewName("index");
        return modelAndView;
    }

    @GetMapping(value = "/viewitem/{id}")
    public ModelAndView editReference(@PathVariable Long id) {
        return index(id);
    }

    @GetMapping(value = "/user/{id}")
    public ModelAndView publishedLinks(@PathVariable String id) {
        ModelAndView modelAndView = new ModelAndView();

        UserPayloadDTO user = serviceCalls.serviceCallUser(id);

        modelAndView.addObject("$id", user.getId());
        modelAndView.addObject("$created", user.getCreated());

        if (Objects.nonNull(user.getSubmitted())) {
            List<HackerPayloadDTO> items = new ArrayList<>();
            int counter = 0;
            for (Long item : user.getSubmitted()) {
                counter++;
                items.add(serviceCalls.serviceCallItem(item));
                if (counter == utils.getMax()) {
                    modelAndView.addObject("$items", items);
                    modelAndView.setViewName("users");
                    return modelAndView;
                }
            }
            modelAndView.addObject("$items", items);
        }

        modelAndView.setViewName("users");
        return modelAndView;
    }


    @GetMapping(value = "/storytype/{id}")
    public ModelAndView storyTypes(@PathVariable String id) {
        ModelAndView modelAndView = new ModelAndView();

        List<Long> stories = serviceCalls.serviceCallStories(id);
        String type = "";

        switch (id) {
            case "topstories":
                type = "Top Stories";
                break;
            case "newstories":
                type = "New Stories";
                break;
            case "beststories":
                type = "Best Stories";
                break;
            case "askstories":
                type = "Ask Stories";
                break;
            case "showstories":
                type = "Sow Stories";
                break;
            case "jobstories":
                type = "Job Stories";
                break;
        }
        if (Objects.nonNull(stories)) {
            List<HackerPayloadDTO> items = new ArrayList<>();
            int counter = 0;
            for (Long item : stories) {
                if (!serviceCalls.serviceCallItem(item).isDead() && !serviceCalls.serviceCallItem(item).isDeleted()) {
                    counter++;
                    items.add(serviceCalls.serviceCallItem(item));
                }
                if (counter == utils.getMax()) {
                    modelAndView.addObject("$type", type);
                    modelAndView.addObject("$items", items);
                    modelAndView.setViewName("storiesgroups");
                    return modelAndView;
                }
            }
        }

        modelAndView.setViewName("storiesgroups");
        return modelAndView;
    }


}
