package za.co.hackherald.services;

import org.junit.Test;
import za.co.hackherald.dtos.HackerPayloadDTO;
import za.co.hackherald.dtos.UpdatesPayloadDTO;
import za.co.hackherald.dtos.UserPayloadDTO;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class ServiceCallsTest {

    @Test
    public void shouldTestServiceCallItem() {
        ServiceCalls serviceCalls = new ServiceCalls();

        HackerPayloadDTO hackerPayloadDTO = serviceCalls.serviceCallItem(1L);

        assertNotNull(hackerPayloadDTO);
    }

    @Test
    public void shouldMockServiceCallUser() {
        ServiceCalls mockServiceCalls = mock(ServiceCalls.class);

        when(mockServiceCalls.serviceCallUser(any())).thenReturn(getUser());

        assertNotNull(mockServiceCalls.serviceCallUser(any()));
        assertEquals(mockServiceCalls.serviceCallUser(any()).getId(), "1000");
    }

    @Test
    public void shouldTestServiceCallMaxItem() {
        ServiceCalls serviceCalls = new ServiceCalls();

        Long aLong = serviceCalls.serviceCallMaxItem();

        assertNotNull(aLong);
    }

    @Test
    public void shouldTestServiceCallStories() {
        ServiceCalls serviceCalls = new ServiceCalls();

        List<Long> longList = serviceCalls.serviceCallStories("topstories");

        assertNotNull(longList);
    }

    @Test
    public void shouldTestServiceCallUpdates() {
        ServiceCalls serviceCalls = new ServiceCalls();

        UpdatesPayloadDTO updatesPayloadDTO = serviceCalls.serviceCallUpdates();

        assertNotNull(updatesPayloadDTO);
    }

    private UserPayloadDTO getUser() {
        UserPayloadDTO userPayloadDTO = new UserPayloadDTO();

        userPayloadDTO.setId("1000");
        userPayloadDTO.setAbout("Human");
        userPayloadDTO.setCreated("1900");

        return userPayloadDTO;
    }
}
